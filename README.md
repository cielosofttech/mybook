Install all the requirements:
	pip install -r requirements.txt

Application creation
	django-admin startproject mysite
    
create submodule
	django-admin startapp module_name / python manage.py startapp myapi
    
mmigrate the models changes
	python manage.py makemigrations
	python manage.py migrate
	python manage.py migrate --run-syncdb
    
Run the application
	python manage.py runserver

User
	usernname: hrabhardwaj@xyz.com
	pwd: cspython50